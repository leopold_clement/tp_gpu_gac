close all;

%Ouverture d'une image au format couleur
ima=single(imread('../Image/ferrari.jpg'));
ima=ima./255;

%Affichage d'une image couleur avec image
figure('name','RGB in','numbertitle','off');image(ima);


%Taille d'une image
taille=size(ima);
display(taille);

ima_r=ima(:,:,1);
ima_g=ima(:,:,2);
ima_b=ima(:,:,3);

%Affichage d'un niveau de couleur de l'image 
figure('name','R','numbertitle','off');imagesc(ima_r);colormap gray  %Niveau de rouge
figure('name','G','numbertitle','off');imagesc(ima_g);colormap gray  %Niveau de vert
figure('name','B','numbertitle','off');imagesc(ima_b);colormap gray  %Niveau de bleu

%Taille d'une image
taille=size(ima);
display(taille);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%tic toc pour mesurer le temps de calcul  
tic;  

eta_r = ima_r ./ sqrt( ima_r.^2 + ima_g.^2 + ima_b.^2);

ima_out = ima.*(eta_r>0.7);

ima_jaune = ima;
ima_jaune(:,:,2) = ima_jaune(:,:,2) + ima_r.*(eta_r>0.7);

toc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



figure('name','RGB out','numbertitle','off');image(ima_out);
figure('name','RGB jaune','numbertitle','off');image(ima_jaune);

%Sauvegarde d'une image au format jpg
imwrite(ima_out,'../Image/ferrari_out.jpg','jpg');
imwrite(ima_jaune,'../Image/ferrari_jaune.jpg','jpg');


%Sauvegarde d'une image au format raw
fid = fopen('../Image/ferrari_out.raw', 'w');
fwrite(fid, ima_out, 'single');
fclose(fid);

fid = fopen('../Image/ferrari_jaune.raw', 'w');
fwrite(fid, ima_jaune, 'single');
fclose(fid);