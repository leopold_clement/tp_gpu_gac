#include "mylib.h"
#include "mylib.cuh"
#include <math.h>

__device__ int getIdx(int i, int j, int cannal, int size_j){
	return cannal+j*3+i*3*size_j;
}

__global__ void kernel_seuillageGPU(unsigned char *d_image_in, unsigned char *d_image_out,int size_j)
{
	float Csum;
	int i, j, k, iFirst, jFirst;

	iFirst = blockIdx.x*BLOCK_SIZE; // num de block dans la grille de block
	jFirst = blockIdx.y*BLOCK_SIZE;

	i = iFirst + threadIdx.x;// recuperer l'identifiant d'un thread dans les blocs
	j = jFirst + threadIdx.y;

	float nr = 0;

nr=d_image_in[2+j*3+i*3*size_j]/sqrtf(d_image_in[0+j*3+i*3*size_j]*d_image_in[0+j*3+i*3*size_j]+d_image_in[1+j*3+i*3*size_j]*d_image_in[1+j*3+i*3*size_j]+d_image_in[2+j*3+i*3*size_j]*d_image_in[2+j*3+i*3*size_j]);

	if(nr > 0.7)
		d_image_out[1+j*3+i*3*size_j] = d_image_in[2+j*3+i*3*size_j];
	else
		d_image_out[1+j*3+i*3*size_j] = d_image_in[1+j*3+i*3*size_j]; 

	d_image_out[0+j*3+i*3*size_j] = d_image_in[0+j*3+i*3*size_j];
	d_image_out[2+j*3+i*3*size_j] = d_image_in[2+j*3+i*3*size_j];


}

__global__ void kernel_cellShadingGPU(unsigned char *d_image_in, unsigned char *d_image_out, int size_j)
{
	int i, j, k, iFirst, jFirst;

	iFirst = blockIdx.x*BLOCK_SIZE; // num de block dans la grille de block
	jFirst = blockIdx.y*BLOCK_SIZE;

	i = iFirst + threadIdx.x;// recuperer l'identifiant d'un thread dans les blocs
	j = jFirst + threadIdx.y;

	int Gx[3][3] = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
	int Gy[3][3] = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};

	int accI = 0;
	int accJ = 0;
	for (int c=0; c<3;c++){
		int tmpI = 0;
		int tmpJ = 0;
		for (int idx_i = -1; idx_i <2; idx_i++)
		for (int idx_j = -1; idx_j <2; idx_j++)
		{
			tmpI+=Gx[idx_j+1][idx_i+1]*d_image_in[(j+idx_i + size_j*(i+idx_j))*3+c];
			tmpJ+=Gy[idx_j+1][idx_i+1]*d_image_in[(j+idx_i + size_j*(i+idx_j))*3+c];
		}
		accI += tmpI;
		accJ += tmpJ;
	}

	int sobel = (accI*accI + accJ*accJ);


	// for(char c = 0; c<3; c++) {
	// 	accI[c] = 0;
	// 	accI[c] += - d_image_in[getIdx(i-1, j-1, c, size_j)] - 2 * d_image_in[getIdx(i, j-1, c, size_j)] - d_image_in[getIdx(i+1, j-1, c, size_j)];
	// 	accI[c] += + d_image_in[getIdx(i-1, j+1, c, size_j)] + 2 * d_image_in[getIdx(i, j+1, c, size_j)] - d_image_in[getIdx(i+1, j+1, c, size_j)];
	// 	accJ[c] = 0;
	// 	accJ[c] += - d_image_in[getIdx(i-1, j-1, c, size_j)] - 2 * d_image_in[getIdx(i-1, j, c, size_j)] - d_image_in[getIdx(i-1, j+1, c, size_j)];
	// 	accJ[c] += + d_image_in[getIdx(i+1, j-1, c, size_j)] + 2 * d_image_in[getIdx(i+1, j, c, size_j)] - d_image_in[getIdx(i+1, j+1, c, size_j)];
	// }

	// int sobel = 0;
	// for(char c = 0; c<3; c++) {
	// 	sobel += (accI[c]>0) ? accI[c] : -accI[c];
	// 	sobel += (accJ[c]>0) ? accJ[c] : -accJ[c];
	// }
	sobel = sobel>>7;
	// sobel = sobel%256;
	//sobel = pow(sobel, 0.5);
	for(char c = 0; c<3; c++) {
		d_image_out[getIdx(i, j, c, size_j)] = (sobel<5) ? d_image_in[getIdx(i, j, c, size_j)] : 0;
	}
}

__global__ void kernel_derrivee(unsigned char *d_image_in_new, unsigned char *d_image_in_old, unsigned char *d_image_out,int size_j){
	int i, j, iFirst, jFirst;

	iFirst = blockIdx.x*BLOCK_SIZE; // num de block dans la grille de block
	jFirst = blockIdx.y*BLOCK_SIZE;

	i = iFirst + threadIdx.x;// recuperer l'identifiant d'un thread dans les blocs
	j = jFirst + threadIdx.y;

	d_image_out[(j + size_j * i)*3] = d_image_in_new[(j + size_j * i)*3] - d_image_in_old[(j + size_j * i)*3];
	d_image_out[(j + size_j * i)*3+1] = d_image_in_new[(j + size_j * i)*3+1] - d_image_in_old[(j + size_j * i)*3+1];
	d_image_out[(j + size_j * i)*3+2] = d_image_in_new[(j + size_j * i)*3+2] - d_image_in_old[(j + size_j * i)*3+2];
}

Mat seuillageGPU( Mat in)
{
	cudaError_t error;
	Mat out;
	out.create(in.rows,in.cols,CV_8UC3);
	
	// allocate host memory
	unsigned char *h_image_in_GPU ;
	h_image_in_GPU=in.data;
	
	/*cudaEvent_t start,stop,start_mem,stop_mem;
	error = cudaEventCreate(&start_mem);
	error = cudaEventCreate(&stop_mem);
	
	error = cudaEventRecord(start, NULL);
	error = cudaEventSynchronize(start);*/
	
	// images on device memoryÍÍÍ
	unsigned char *d_image_in_GPU;
	unsigned char *d_image_out_GPU;
	
	const unsigned long int mem_size=in.cols*in.rows*3*sizeof(unsigned char);
	
	// Alocation mémoire de d_image_in et d_image_out sur la carte GPU
	cudaMalloc((void**) &d_image_in_GPU,mem_size );
	cudaMalloc((void**) &d_image_out_GPU, mem_size);
	
	// copy host memory to device
	cudaMemcpy(d_image_in_GPU, h_image_in_GPU,mem_size ,cudaMemcpyHostToDevice);
	
	//error = cudaEventRecord(stop_mem, NULL);
	
	// Wait for the stop event to complete
	//error = cudaEventSynchronize(stop_mem);
	//float msecMem = 0.0f;
	//error = cudaEventElapsedTime(&msecMem, start, stop_mem);
	
	// setup execution parameters -> découpage en threads
	dim3 threads(BLOCK_SIZE,BLOCK_SIZE);
	dim3 grid(in.rows/BLOCK_SIZE,in.cols/BLOCK_SIZE);
	
	// lancement des threads executé sur la carte GPU
	kernel_seuillageGPU<<< grid, threads >>>(d_image_in_GPU, d_image_out_GPU,in.cols);
	
	// Record the start event
	//error = cudaEventRecord(start_mem, NULL);
	//error = cudaEventSynchronize(start_mem);
	
	// copy result from device to host
	cudaMemcpy(out.data, d_image_out_GPU, mem_size,cudaMemcpyDeviceToHost);
	cudaFree(d_image_in_GPU);
	cudaFree(d_image_out_GPU);
	/*
	float msecTotal,msecMem2;
	error = cudaEventRecord(stop, NULL);
	error = cudaEventSynchronize(stop);
	error = cudaEventElapsedTime(&msecTotal, start, stop);
	error = cudaEventElapsedTime(&msecMem2, start_mem, stop);
	*/
	return out;
}

Mat derrive(Mat nouvelle, Mat ancienne){
	cudaError_t error;
	Mat out;
	out.create(nouvelle.rows,nouvelle.cols,CV_8UC3);
	
	// allocate host memory
	unsigned char *h_image_in_new_GPU ;
	h_image_in_new_GPU=nouvelle.data;
	unsigned char *h_image_in_old_GPU ;
	h_image_in_old_GPU=nouvelle.data;

	/*cudaEvent_t start,stop,start_mem,stop_mem;
	error = cudaEventCreate(&start_mem);
	error = cudaEventCreate(&stop_mem);
	
	error = cudaEventRecord(start, NULL);
	error = cudaEventSynchronize(start);*/
	
	// images on device memoryÍÍÍ
	unsigned char *d_image_in_new_GPU;
	unsigned char *d_image_in_old_GPU;
	unsigned char *d_image_out_GPU;
	
	const unsigned long int mem_size=nouvelle.cols*nouvelle.rows*3*sizeof(unsigned char);
	
	// Alocation mémoire de d_image_in et d_image_out sur la carte GPU
	cudaMalloc((void**) &d_image_in_new_GPU,mem_size );
	cudaMalloc((void**) &d_image_in_old_GPU,mem_size );
	cudaMalloc((void**) &d_image_out_GPU, mem_size);
	
	// copy host memory to device
	cudaMemcpy(d_image_in_new_GPU, h_image_in_new_GPU, mem_size ,cudaMemcpyHostToDevice);
	cudaMemcpy(d_image_in_old_GPU, h_image_in_old_GPU, mem_size ,cudaMemcpyHostToDevice);
	
	//error = cudaEventRecord(stop_mem, NULL);
	
	// Wait for the stop event to complete
	//error = cudaEventSynchronize(stop_mem);
	//float msecMem = 0.0f;
	//error = cudaEventElapsedTime(&msecMem, start, stop_mem);
	
	// setup execution parameters -> découpage en threads
	dim3 threads(BLOCK_SIZE,BLOCK_SIZE);
	dim3 grid(nouvelle.rows/BLOCK_SIZE,nouvelle.cols/BLOCK_SIZE);
	
	// lancement des threads executé sur la carte GPU
	kernel_derrivee<<< grid, threads >>>(d_image_in_new_GPU, d_image_in_old_GPU, d_image_out_GPU, nouvelle.cols);
	
	// Record the start event
	//error = cudaEventRecord(start_mem, NULL);
	//error = cudaEventSynchronize(start_mem);
	
	// copy result from device to host
	cudaMemcpy(out.data, d_image_out_GPU, mem_size,cudaMemcpyDeviceToHost);
	cudaFree(d_image_in_new_GPU);
	cudaFree(d_image_in_old_GPU);
	cudaFree(d_image_out_GPU);
	/*
	float msecTotal,msecMem2;
	error = cudaEventRecord(stop, NULL);
	error = cudaEventSynchronize(stop);
	error = cudaEventElapsedTime(&msecTotal, start, stop);
	error = cudaEventElapsedTime(&msecMem2, start_mem, stop);
	*/
	return out;
}

Mat cellShadingGPU( Mat in)
{
	cudaError_t error;
	Mat out;
	out.create(in.rows,in.cols,CV_8UC3);
	
	// allocate host memory
	unsigned char *h_image_in_GPU ;
	h_image_in_GPU=in.data;
	
	/*cudaEvent_t start,stop,start_mem,stop_mem;
	error = cudaEventCreate(&start_mem);
	error = cudaEventCreate(&stop_mem);
	
	error = cudaEventRecord(start, NULL);
	error = cudaEventSynchronize(start);*/
	
	// images on device memoryÍÍÍ
	unsigned char *d_image_in_GPU;
	unsigned char *d_image_out_GPU;
	
	const unsigned long int mem_size=in.cols*in.rows*3*sizeof(unsigned char);
	
	// Alocation mémoire de d_image_in et d_image_out sur la carte GPU
	cudaMalloc((void**) &d_image_in_GPU,mem_size );
	cudaMalloc((void**) &d_image_out_GPU, mem_size);
	
	// copy host memory to device
	cudaMemcpy(d_image_in_GPU, h_image_in_GPU,mem_size ,cudaMemcpyHostToDevice);
	
	//error = cudaEventRecord(stop_mem, NULL);
	
	// Wait for the stop event to complete
	//error = cudaEventSynchronize(stop_mem);
	//float msecMem = 0.0f;
	//error = cudaEventElapsedTime(&msecMem, start, stop_mem);
	
	// setup execution parameters -> découpage en threads
	dim3 threads(BLOCK_SIZE,BLOCK_SIZE);
	dim3 grid(in.rows/BLOCK_SIZE,in.cols/BLOCK_SIZE);
	
	// lancement des threads executé sur la carte GPU
	kernel_cellShadingGPU<<< grid, threads >>>(d_image_in_GPU, d_image_out_GPU,in.cols);
	
	// Record the start event
	//error = cudaEventRecord(start_mem, NULL);
	//error = cudaEventSynchronize(start_mem);
	
	// copy result from device to host
	cudaMemcpy(out.data, d_image_out_GPU, mem_size,cudaMemcpyDeviceToHost);
	cudaFree(d_image_in_GPU);
	cudaFree(d_image_out_GPU);
	/*
	float msecTotal,msecMem2;
	error = cudaEventRecord(stop, NULL);
	error = cudaEventSynchronize(stop);
	error = cudaEventElapsedTime(&msecTotal, start, stop);
	error = cudaEventElapsedTime(&msecMem2, start_mem, stop);
	*/
	return out;
}
